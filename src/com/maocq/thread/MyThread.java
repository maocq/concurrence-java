package com.maocq.thread;

public class MyThread extends Thread {

	public MyThread() {
	}

	@Override
	public void run() {
		try {
			Thread.sleep(2000);
			System.out.println("Hello Thread");
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}
	}

}
