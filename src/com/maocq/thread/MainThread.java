package com.maocq.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainThread {

	public static final int THREADPOOL = 10;

	public static void main(String[] args) {

		// MyThread myThread = new MyThread();
		// myThread.start();
		// System.out.println(" END");

		ExecutorService executorService = Executors.newFixedThreadPool(THREADPOOL);
		executorService.submit(new MyThread());
		executorService.shutdown();
		System.out.println("ESPERANDO ...");
		while (!executorService.isTerminated()) {
		}
		System.out.println(" END");

	}
}
