package com.maocq.runnable;

public class MyRunnable implements Runnable{
	
	public MyRunnable() {
	}

	@Override
	public void run() {
		try {
			Thread.sleep(2000);
			System.out.println("Hello Runnable");
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}		
	}

}
