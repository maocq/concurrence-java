package com.maocq.runnable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainRunnable {

	public static final int THREADPOOL = 10;

	public static void main(String[] args) {

		// MyRunnable myRunnable = new MyRunnable();
		// new Thread(myRunnable).start();
		// System.out.println(" END");

		ExecutorService executorService = Executors.newFixedThreadPool(THREADPOOL);
		// executorService.submit(new MyRunnable());
		executorService.execute(new MyRunnable());

		// Cierre del ejecutor
		executorService.shutdown();
		System.out.println("ESPERANDO ...");
		while (!executorService.isTerminated()) {
		}
		System.out.println(" END");

	}

}
