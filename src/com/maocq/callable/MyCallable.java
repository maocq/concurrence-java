package com.maocq.callable;

import java.util.concurrent.Callable;

public class MyCallable implements Callable<String> {

	public MyCallable() {
	}

	@Override
	public String call() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}
		return "Hello Callable";
	}
}
