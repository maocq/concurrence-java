package com.maocq.callable;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MainCallable {

	public static final int THREADPOOL = 10;
	
	public static void main(String[] args) {
		
		ExecutorService executorService = Executors.newFixedThreadPool(THREADPOOL);
		
		// MyCallable myCallable = new MyCallable();
		// Future<String> future = executorService.submit(myCallable);
		
		Future<String> future = executorService.submit(() -> {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
			return "Hello Callable";
		});		
		
		System.out.println("ESPERANDO ...");
		
		//future.isDone();
		
		try {
			String response = future.get();
			System.out.println(response);
		} catch (InterruptedException | ExecutionException e) {
			System.out.println(e.getMessage());
		}
		System.out.println(" END");
	}

}
